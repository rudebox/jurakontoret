<?php

/* Do not remove this line. */
require_once('includes/scratch.php');
require_once('lib/theme-dependencies.php');

// ini_set('xdebug.max_nesting_level', 50000);


/*
 * adds all meta information to the <head> element for us.
 */

function scratch_meta() { ?>

  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" href="/apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

<?php }

add_action('wp_head', 'scratch_meta');

/* Theme CSS */

function theme_styles() {

  wp_enqueue_style( 'normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css', false, null );

  wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.8.2/css/all.css', false, null );

  wp_register_style( 'scratch-main', get_template_directory_uri() . '/assets/css/master.css', false, filemtime(dirname(__FILE__) . '/assets/css/master.css') );
  wp_enqueue_style( 'scratch-main' );

}

add_action( 'wp_enqueue_scripts', 'theme_styles' );

/* Theme JavaScript */

function theme_js() {

  // wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-2.8.3.min.js', false, false, false );

  wp_register_script( 'scratch-main-concat', get_template_directory_uri() . '/assets/js/concat/main.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/concat/main.js'), true ); 

  wp_register_script( 'scratch-main-min', get_template_directory_uri() . '/assets/js/build/main.min.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/build/main.min.js'), true );


  if (!is_admin()) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js', array(), null, true );
  }

  /* FOR DEVELOPMENT */
  // wp_enqueue_script( 'scratch-main-concat' );

  /* FOR PRODUCTION */
  wp_enqueue_script( 'scratch-main-min' );

}

add_action( 'wp_enqueue_scripts', 'theme_js' );


/* Enable ACF Options Pages */


if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Globalt indhold',
    'menu_title'  => 'Globalt indhold',
    'menu_slug'   => 'global-content',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Header',
    'menu_title'  => 'Header',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Sidebar',
    'menu_title'  => 'Sidebar',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Menu',
    'menu_title'  => 'Menu',
    'parent_slug' => 'global-content',
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Footer',
    'menu_title'  => 'Footer',
    'parent_slug' => 'global-content',
  ));
}

/* Enable Featured Image */

add_theme_support( 'post-thumbnails' );

/* Enable Custom Menus */

add_theme_support( 'menus' );

register_nav_menus(
  array(
    'scratch-main-nav' => __( 'Main Nav', 'scratch' )   // main nav in header
  )
);

function scratch_main_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Main Nav', 'scratch' ), // nav name
    'menu_class' => 'nav__menu', // adding custom nav class
    'theme_location' => 'scratch-main-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end scratch main nav */


// Image sizes
add_image_size('link-box', 800, 546, true);

// Add created images sizes to dropdown in WP control panel
add_filter( 'image_size_names_choose', 'custom_image_sizes' );

function custom_image_sizes( $sizes ) {
  return array_merge( $sizes, array(
    // '' => __( '' )
  ) );
}  


// Register Custom Post Type Brevkasse
function create_brevkasse_cpt() {

  $labels = array(
    'name' => _x( 'Brevkasse', 'Post Type General Name', 'lionlab' ),
    'singular_name' => _x( 'Brevkasse', 'Post Type Singular Name', 'lionlab' ),
    'menu_name' => _x( 'Brevkasse', 'Admin Menu text', 'lionlab' ),
    'name_admin_bar' => _x( 'Brevkasse', 'Add New on Toolbar', 'lionlab' ),
    'archives' => __( 'Brevkasse Archives', 'lionlab' ),
    'attributes' => __( 'Brevkasse Attributes', 'lionlab' ),
    'parent_item_colon' => __( 'Parent Brevkasse:', 'lionlab' ),
    'all_items' => __( 'Alle Brevkasse indlæg', 'lionlab' ),
    'add_new_item' => __( 'Tilføj nyt indlæg til Brevkasse', 'lionlab' ),
    'add_new' => __( 'Tilføj ny', 'lionlab' ),
    'new_item' => __( 'New Brevkasse', 'lionlab' ),
    'edit_item' => __( 'Redigere Brevkasse indlæg', 'lionlab' ),
    'update_item' => __( 'Update Brevkasse', 'lionlab' ),
    'view_item' => __( 'View Brevkasse', 'lionlab' ),
    'view_items' => __( 'View Brevkasse', 'lionlab' ),
    'search_items' => __( 'Search Brevkasse', 'lionlab' ),
    'not_found' => __( 'Not found', 'lionlab' ),
    'not_found_in_trash' => __( 'Not found in Trash', 'lionlab' ),
    'featured_image' => __( 'Featured Image', 'lionlab' ),
    'set_featured_image' => __( 'Set featured image', 'lionlab' ),
    'remove_featured_image' => __( 'Remove featured image', 'lionlab' ),
    'use_featured_image' => __( 'Use as featured image', 'lionlab' ),
    'insert_into_item' => __( 'Insert into Brevkasse', 'lionlab' ),
    'uploaded_to_this_item' => __( 'Uploaded to this Brevkasse', 'lionlab' ),
    'items_list' => __( 'Brevkasse list', 'lionlab' ),
    'items_list_navigation' => __( 'Brevkasse list navigation', 'lionlab' ),
    'filter_items_list' => __( 'Filter Brevkasse list', 'lionlab' ),
  );
  $args = array(
    'label' => __( 'Brevkasse', 'lionlab' ),
    'description' => __( '', 'lionlab' ),
    'labels' => $labels,
    'menu_icon' => 'dashicons-archive',
    'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author', 'comments', 'trackbacks', 'page-attributes', 'post-formats', 'custom-fields'),
    'taxonomies' => array('category'),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'show_in_admin_bar' => true,
    'show_in_nav_menus' => true,
    'can_export' => true,
    'has_archive' => true,
    'hierarchical' => false,
    'exclude_from_search' => false,
    'show_in_rest' => true,
    'publicly_queryable' => true,
    'capability_type' => 'post',
  );
  register_post_type( 'brevkasse', $args );

}
add_action( 'init', 'create_brevkasse_cpt', 0 );


//make gf submit input to button
function gf_make_submit_input_into_a_button_element($button_input, $form) {

  //save attribute string to $button_match[1]
  preg_match("/<input([^\/>]*)(\s\/)*>/", $button_input, $button_match);

  //remove value attribute
  $button_atts = str_replace("value='".$form['button']['text']."' ", "", $button_match[1]);

  return '<button '.$button_atts.'><span>'.$form['button']['text'].'</span></button>';
}
add_filter('gform_submit_button', 'gf_make_submit_input_into_a_button_element', 10, 2);


//gravity form scripts in footer
add_filter('gform_init_scripts_footer', 'init_scripts');
  function init_scripts() {
  return true;
}


//gravity form styles in header
add_action('wp_enqueue_scripts', function() {
    if (function_exists('gravity_form_enqueue_scripts')) {
        // newsletter subscription form
        gravity_form_enqueue_scripts(5);
    }
});


//send notification to admins on created post
add_action('acf/pre_save_post', 'send_mail_on_new_post_created');

function send_mail_on_new_post_created( $post_id ) {

  if( get_post_type($post_id) !== 'brevkasse' ) {
    return;
  }

  if( is_admin() ) {
    return;
  }

  $post_title = get_the_title( $post_id );
  $post_url   = get_permalink( $post_id );
  $subject  = "Nyt indlæg til brevkasse er indsendt";
  $message  = "Gennemlæs spørgsmålet og tilføj et svar og en passende kategori inden du offentliggører indlægget:\n\n";
  $message   .= $post_title . ": " . $post_url;

  $administrators   = get_users(array(
    'role'  => 'administrator'
  ));

  foreach ($administrators as &$administrator) {
    wp_mail( $administrator->data->user_email, $subject, $message );
  }

}


//save category from frontend to post
function acf_review_before_save_post($post_id) {
  if (empty($_POST['acf']))
    return;
  $_POST['acf']['_post_category'] = $_POST['acf']['field_5cee9f44f1b23'];
  return $post_id;

}

add_action('acf/pre_save_post', 'acf_review_before_save_post');


//Returns pagination element with posibility of using a custom query

function lionlab_pagination_hook( $query ) {

  // Fallback to global query
  if ($query == null) {
    global $wp_query;
    $query = $wp_query;
  }

  // Need an unlikely integer
  $big = 999999999;

  // Arguments
  $args = array(
    'base'               => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format'             => '?paged=%#%',
    'total'              => $query->max_num_pages,
    'current'            => max( 1, get_query_var('paged') ),
    'show_all'           => false,
    'end_size'           => 1,
    'mid_size'           => 1,
    'prev_next'          => false,
    'type'               => 'plain',
  );

  // Only display if more than 1 page
  if ( intval($query->max_num_pages) > 1 ) {
    ob_start() ?>
    
    <div class="pagination flex flex--valign">
      <div class="pagination__nav pagination__previous left"><?php previous_posts_link('<i class="fas fa-angle-left"></i>'); ?></div>
    
        <div class="pagination__pages"><?php echo paginate_links($args); ?></div>

      <div class="pagination__nav pagination__next right"><?php next_posts_link('<i class="fas fa-angle-right"></i>'); ?></div>
    </div>

    <?php
    echo ob_get_clean();
  }
}

add_action( 'lionlab_pagination', 'lionlab_pagination_hook' );


//rewrite next and previous post links class
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return 'class="pagination__icons"';
}


//force categories of custom post type use default archive too
function lionlab_show_cpt_archives( $query ) {
 if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
   $query->set( 'post_type', array(
   'post', 'nav_menu_item', 'brevkasse'
 ));

 return $query;
 }
}

add_filter('pre_get_posts', 'lionlab_show_cpt_archives');

function lionlab_pagination_rewrite() {
    

    // $cat = single_cat_title();

    // $terms = get_terms( array(
    //     'taxonomy' => $cat,
    //     'hide_empty' => false,
    // ) );

    // $current_slug = get_page_template_slug( $post->ID );

    // var_dump($terms[0]->slug);

    // add_rewrite_rule($terms[0]->slug .  '/page/?([0-9]{1,})/?$', 'index.php?category_name=' . $terms[0]->slug . '&paged=$matches[1]', 'top');

    //Fixing pagination 404 error - remember to update pernalinks for effects to be applied
    //aint the best fix as it not very dynamic - will try to work something better out.

    add_rewrite_rule('koeb-af-fast-ejendom/page/?([0-9]{1,})/?$', 'index.php?category_name=koeb-af-fast-ejendom&paged=$matches[1]', 'top'); 

    add_rewrite_rule('andre-retsomraader/page/?([0-9]{1,})/?$', 'index.php?category_name=andre-retsomraader&paged=$matches[1]', 'top');

    add_rewrite_rule('lejeret/page/?([0-9]{1,})/?$', 'index.php?category_name=lejeret&paged=$matches[1]', 'top');

    add_rewrite_rule('testamenter-arv-og-skilsmisse/page/?([0-9]{1,})/?$', 'index.php?category_name=testamenter-arv-og-skilsmisse&paged=$matches[1]', 'top');
}

add_action('init', 'lionlab_pagination_rewrite');


//accordion shortcode
  function fold_shortcode( $atts , $content = null ) {

      $atts = shortcode_atts(
        array(
          'titel' => '',
        ),
        $atts
      );

    return '<div class="accordion__wrapper"><h3 class="accordion">' . $atts['titel'] . '</h3><div class="panel">' . $content . '</div></div>';

  }

  add_shortcode( 'fold', 'fold_shortcode' );
                            

/* Place custom functions below here. */

/* Don't delete this closing tag. */
?>
