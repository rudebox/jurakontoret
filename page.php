<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <section class="page padding--bottom">

    <div class="wrap hpad">

      <article id="post-<?php the_ID(); ?>"
               <?php post_class(); ?>>

        <header>
          <h1><?php the_title(); ?></h1>
        </header>

        <?php the_content(); ?>

      </article>

    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>