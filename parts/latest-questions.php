<?php 
	// Query loop for custom post types
	$args_query = array(
		'posts_per_page' => 3,
		'order' => 'DESC',
		'post_type' => 'brevkasse',
       	'post_status' => 'publish',
	);

	$query = new WP_Query( $args_query );


	//section settings
	$bg = get_sub_field('bg');
	$margin = get_sub_field('margin');
	$text = get_sub_field('text');
 ?>

 <section class="archive <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
 	<div class="wrap hpad">

 		<h2 class="archive__header"><?php echo esc_html(the_sub_field('header')); ?></h2>
 		<?php echo _e($text); ?>

		<?php if ($query->have_posts()): ?>
		  <?php while ($query->have_posts()): $query->the_post(); ?>

		  <?php 
          //Get category id to match up with archive filter
          $cats = get_the_category();

          foreach ($cats as $cat) {
            
          } 
          ?>

		   <article class="archive__item" itemscope itemtype="http://schema.org/BlogPosting">

		    <header class="archive__header">
		      <a href="<?php echo the_permalink(); ?>">
		      	<h2 class="archive__title h3" title="<?php the_title_attribute(); ?>">
		          <?php the_title(); ?>                
		      	</h2>
		      </a>
		    </header>

		    <div class="archive__body">
		      <?php the_excerpt(); ?>
		    </div>

		    <div class="archive__meta">
		      <strong>Kategori: </strong><a class="archive__meta--link" href="<?php echo get_category_link($cat); ?>"><?php echo $cat->name; ?></a>
		    </div>

		    <div class="archive__footer">
		      <a class="btn--icon arhive__btn" href="<?php the_permalink(); ?>">Læs mere <i class="fas fa-angle-right"></i></a>
		    </div>

		  </article>

			<?php endwhile; else: ?>

		  <p>No posts here.</p>

		<?php endif; ?>

		<?php wp_reset_postdata(); ?>

	</div>
</section>