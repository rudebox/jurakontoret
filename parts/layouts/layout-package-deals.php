<?php 
/**
* Description: Lionlab package-deals field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$intro = get_sub_field('header_intro');

//package deals
$package_title = get_sub_field('package_title');
$package_text = get_sub_field('package_text');
?>

<section class="package-deals <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="package-deals__header center"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<?php if ($intro) : ?>
		<div class="center package-deals__intro"><?php echo esc_html($intro); ?></div>
		<?php endif; ?>
		<div class="row flex flex--wrap">

			<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 package-deals__item center">
				<div class="package-deals__table-header"><h5 class="package-deals__table-title"><?php echo esc_html($package_title); ?></h5></div>
				<div class="package-deals__content">
					<?php echo $package_text; ?>
				</div>
			</div>

		</div>
	</div>
</section>