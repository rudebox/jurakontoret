<?php 
/**
* Description: Lionlab quicklinks repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

if (have_rows('quicklink') ) :
?>

<section class="quicklinks <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">
			<?php while (have_rows('quicklink') ) : the_row(); 
				$text = get_sub_field('text');				
				$link = get_sub_field('link');
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-4 quicklinks__item">
				<h4 class="quicklinks__link flex flex--center flex--justify"><?php echo $text; ?> <i class="fas fa-angle-right"></i></h4>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>