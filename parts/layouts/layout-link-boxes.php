<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('linkbox') ) :
?>

<section class="link-boxes <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">
		<h2 class="link-boxes__header center"><?php echo esc_html($title); ?></h2>
		<div class="row flex flex--wrap">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$icon = get_sub_field('icon');
				$link = get_sub_field('link');
			?>

			<div class="col-sm-3 link-boxes__item">
				<?php if ($icon) : ?>
				<img src="<?php echo esc_url($icon['sizes']['link-box']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
				<?php endif; ?>
				<h3 class="link-boxes__title"><?php echo esc_html($title); ?></h3>
				<p><?php echo $text; ?></p>

				<?php  if ($link) : ?>
				<a class="btn btn--hollow link-boxes__btn" href="<?php echo esc_url($link); ?>">Læs mere</a>
				<?php endif; ?>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>