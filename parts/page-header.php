<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden blev ikke fundet', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//trim title for custom post_type brevkasse
	$short_title = wp_trim_words( $title, 6, '...' );
?>


<?php 
	//hero img with falllback
	$img = get_field('page_img') ? : $img = get_field('page_img', 'options'); 

	//get categories
	$cats = get_the_category();
?>


<section class="page__hero padding--both" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
	<div class="wrap hpad center">
		<?php if (!is_singular('brevkasse') ) : ?>
		<h1 class="page__title"><?php echo esc_html($title); ?></h1>
		<?php else : ?>
		<h2 class="page__title h1"><?php echo esc_html($short_title); ?></h2>
		
		<?php foreach ($cats as $cat) :?>
			<div class="page__meta"><?php echo $cat->name; ?></div>
		<?php endforeach; ?>

		<?php endif; ?>
	</div>
</section>