<section class="archive__filter gray--bg">
	<div class="wrap hpad flex flex--center flex--justify flex--wrap">
	  <h3 class="archive__filter--title">Emner</h3>
	
	  <div class="archive__select--wrapper">
		<form id="#js-archive-form" class="archive__form" action="<?php echo esc_url( home_url( '/' ) ); ?><?php echo $slug; ?>" method="get">

			<?php

				$args = array(
					'show_option_all' => __( 'Alle kategorier' ),
					'show_count'       => 1,
					'orderby'          => 'name',
					'echo'             => 0,
					'class'			   => 'archive__select',
					'id'			   => 'js-archive-dropdown',
				);


				$slug = array (
					'value_field'	   => 'slug',
				);
			?>

			<?php $select  = wp_dropdown_categories( $args ); ?>
			<?php $replace = "<select$1 onchange='return this.form.submit()'>"; ?>
			<?php $select  = preg_replace( '#<select([^>]*)>#', $replace, $select ); ?>

			<?php echo $select; ?>

			<noscript>
				<input type="submit" value="View" />
			</noscript>

		</form>

	  </div>
	</div>
</section>
