<?php

/*
 * Template Name: Stil spørgsmål
 */

acf_form_head(); 
get_template_part('parts/header'); 
?>

<main>
	<?php get_template_part('parts/page', 'header');?>
	<section class="submit padding--both">
		<div class="wrap hpad"> 

			<div class="submit__postbox">

				<?php 

					acf_form(array(
						'post_id'		=> 'new_post',
						'post_title'	=> true,
						'post_content'	=> true,
						'post_category'	=> $_POST['acf']['field_5cee9f44f1b23'],
						'honeypot' 		=> true,
						'new_post'		=> array(
							'post_type'		=> 'brevkasse', 
							'post_status'	=> 'pending'
						),
						'html_submit_button'	=> '<button type="submit" class="acf-button button btn btn--hollow btn--hollow--default-text submit__btn">Indsend mit spørgsmål</button>',
						'html_updated_message'	=> '<div id="message"class="updated"><p>Tak for dit for dit spørgsmål. Af hensyn til spam kan der gå noget tid før indlægget bliver offentliggjort. Hav venligst tålmodighed.</p></div>',
					));

				?>

			</div>

		</div>
	</div>

</main>

<?php get_template_part('parts/footer'); ?>



