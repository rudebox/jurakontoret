jQuery(document).ready(function($) {

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
    $('#overlay').toggleClass('is-visible');
  });

  $('#overlay').on('click', function() {
    $('.nav--mobile').removeClass('is-open');
    $('body').removeClass('is-fixed');
    $('#overlay').removeClass('is-visible');
    $('.nav__item').removeClass('is-animated');
  });

  if ($('#overlay').hasClass('is-visible')) {

  }


  //wrap table for responsiveness
  $('.table').wrap('<div class="table-wrapper">');


  //owl slider/carousel
  var owl = $(".slider__track");

  owl.each(function() {
    $(this).children().length > 1;

      $(this).owlCarousel({
        loop: false,
        items: 1,
        autoplay: true,
        // nav: true,
        autplaySpeed: 11000,
        autoplayTimeout: 10000,
        smartSpeed: 250,
        smartSpeed: 2200,
        navSpeed: 2200
        // navText : ["<i class='fa fa-arrow-left' aria-hidden='true'></i>", "<i class='fa fa-arrow-right' aria-hidden='true'></i>"]
      });
  });


  //wrap btn text
  $('.btn--hollow').wrapInner('<span></span>');

  $('.nav-toggle').click(function() {
    $('.nav__item').each(function(index){        
        var delayNumber = index * 100;

        $(this).delay(delayNumber).queue(function(next) {
          $(this).toggleClass('is-animated');
          next();
        });  
    })
  }); 
  

  //translation hack of ACF frontend form field labels - beacause WPML is overkill
  $('.acf-field-wysiwyg label').each(function() {
    var text = $(this).text();
    $(this).text(text.replace('Content', 'Tekst indhold')); 
  });

  $('.acf-field-text label').each(function() {
    var text = $(this).text();
    $(this).text(text.replace('Title', 'Overskrift på dit spørgsmål')); 
  });

  //replace placeholde text
  $('#acf-field_5cee9f44f1b23').attr('data-placeholder', 'Vælg kategori');


  //Hide frontend form when submitted  
  if (window.location.href.indexOf('?updated=true') > -1) {
    $('.acf-form').css({
      'display': 'none'
    }); 
  } 


  //redirect to all categories if all categories selected 
  $('.archive__select').change(function()  {
    if ($(this).val() === '0') {
      window.location.replace('/brevkasse');
    }
  });


   //toggle accordion
  var $acc = $('.accordion');

    $acc.each(function() {
      $(this).click(function() {
        $(this).toggleClass('is-active');
        $(this).next().toggleClass('is-visible');
    });
  });

});
