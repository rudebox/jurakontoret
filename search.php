<?php get_template_part('parts/header'); ?>

<main itemscope itemtype="http://schema.org/SearchResultsPage">

<?php get_template_part('parts/page', 'header'); ?>

   <section class="archive padding--both">
    <div class="wrap hpad">

      <p class="center">
        <strong>Søgeresultat for:</strong>
        <?php echo esc_attr(get_search_query()); ?>
      </p>
      
      <?php if (have_posts()): ?>
        <?php while (have_posts()): the_post(); ?>

        <?php 
          //Get category id to match up with archive filter
          $cats = get_the_category();
          $cat_string = '';

          foreach ($cats as $cat) {
            
          } 
        ?>

        <article class="archive__item" itemscope itemtype="http://schema.org/BlogPosting">

          <header class="archive__header">
            <a href="<?php echo the_permalink(); ?>">
              <h2 class="archive__title h3" title="<?php the_title_attribute(); ?>">
                <?php the_title(); ?>                
              </h2>
            </a>
          </header>

          <div class="archive__body">
            <?php the_excerpt(); ?>
          </div>
          
          <?php if (in_category(array('7', '5', '3', '6', '4') ) ) : ?>
          <div class="archive__meta">
            <strong>Kategori: </strong><a class="archive__meta--link" href="<?php echo get_category_link($cat); ?>"><?php echo $cat->name; ?></a>
          </div>
          <?php endif; ?>

          <div class="archive__footer">
            <a class="btn--icon arhive__btn" href="<?php the_permalink(); ?>">Læs mere <i class="fas fa-angle-right"></i></a>
          </div>

        </article>

        <?php endwhile; else: ?>

          <p>No posts here.</p>

      <?php endif; ?>
    
    </div>

    <div class="clearfix"></div>
    
    <div class="wrap hpad">
      <?php 
        // do pagination
        do_action( 'lionlab_pagination' );
      ?>
    </div>
    
  </section>

</main>

<?php get_template_part('parts/footer'); ?>