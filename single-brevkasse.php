<?php get_template_part('parts/header'); the_post(); ?>

<main>
<?php get_template_part('parts/page', 'header'); ?>

  <section class="single padding--both">

  	<div class="wrap hpad">

	    <article class="single__item" itemscope itemtype="http://schema.org/BlogPosting">

	      <header class="single__header">
	        <h1 class="single__title h2"><?php echo the_title(); ?></h1>
	      </header>

	      <div class="single__question" itemprop="articleBody">
          <?php the_content(); ?>
        </div>

        <?php $answer = get_field('answer'); ?>
          
        <div class="single__answer">
          <strong>Svar:</strong>
          <?php echo $answer; ?> 

          <a class="btn btn--hollow btn--hollow--default-text single__btn" onclick="window.history.go(-1); return false;">Tilbage</a> 
        </div>


	    </article>

    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>